import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:flame/effects.dart';
import 'package:flutter/services.dart';
import 'package:platform_game/actors/water_enemy.dart';
import 'package:platform_game/ember_quest.dart';
import 'package:platform_game/objects/ground_block.dart';
import 'package:platform_game/objects/platform_block.dart';
import 'package:platform_game/objects/star.dart';

class EmberPlayer extends SpriteAnimationComponent
    with CollisionCallbacks, KeyboardHandler, HasGameRef<EmberQuestGame> {
  EmberPlayer({required super.position})
      : super(size: Vector2.all(64), anchor: Anchor.center);

  int horizontDirection = 0;
  final Vector2 velocity = Vector2.zero();
  final double moveSpeed = 200;
  final Vector2 fromAbove = Vector2(0, -1);
  bool isOnGround = false;

  //gravity
  final double gravity = 15;
  final double jumpSpeed = 600;
  final double terminalVelocity = 150;

  bool hasJumped = false;

  //enemy
  bool hitByEnemy = false;

  void hit() {
    if (!hitByEnemy) {
      game.health--;
      hitByEnemy = true;
    }
    add(OpacityEffect.fadeOut(
      EffectController(alternate: true, duration: 0.1, repeatCount: 5),
    )..onComplete = () {
        hitByEnemy = false;
      });
  }

  @override
  Future<void>? onLoad() {
    // TODO: implement onLoad
    animation = SpriteAnimation.fromFrameData(
        game.images.fromCache('ember.png'),
        SpriteAnimationData.sequenced(
            amount: 4, stepTime: 0.12, textureSize: Vector2.all(16)));
    add(CircleHitbox());
    return super.onLoad();
  }

  @override
  bool onKeyEvent(RawKeyEvent event, Set<LogicalKeyboardKey> keysPressed) {
    horizontDirection = 0;
    horizontDirection += (keysPressed.contains(LogicalKeyboardKey.keyA) ||
            keysPressed.contains(LogicalKeyboardKey.arrowLeft) && !game.loadHubState)
        ? -1
        : 0;
    horizontDirection += (keysPressed.contains(LogicalKeyboardKey.keyD) ||
            keysPressed.contains(LogicalKeyboardKey.arrowRight) &&
                !game.loadHubState)
        ? 1
        : 0;
    hasJumped = keysPressed.contains(LogicalKeyboardKey.space) && !game.loadHubState;
    return true;
  }

  @override
  void update(double dt) {
    // TODO: implement update
    velocity.x = horizontDirection * moveSpeed;
    game.objectSpeed = 0;

    if (position.x - 36 <= 0 && horizontDirection < 0) {
      velocity.x = 0;
    }

    if (position.x + 64 >= game.size.x / 2 && horizontDirection > 0) {
      velocity.x = 0;
      game.objectSpeed = -moveSpeed;
    }

    if (horizontDirection < 0 && scale.x > 0) {
      flipHorizontally();
    } else if (horizontDirection > 0 && scale.x < 0) {
      flipHorizontally();
    }

    velocity.y += gravity;

    if (hasJumped) {
      if (isOnGround) {
        velocity.y = -jumpSpeed;
        isOnGround = false;
      }
      hasJumped = false;
    }
    velocity.y = velocity.y.clamp(-jumpSpeed, terminalVelocity);
    position += velocity * dt;

    if (position.y > game.size.y + size.y) {
      game.health = 0;
    }
    if (game.health <= 0) {
      removeFromParent();
    }
    super.update(dt);
  }

  @override
  void onCollision(Set<Vector2> intersectionPoints, PositionComponent other) {
    // TODO: implement onCollision
    if (other is GroundBlock || other is PlatformBlock) {
      if (intersectionPoints.length == 2) {
        final mid = (intersectionPoints.elementAt(0) +
                intersectionPoints.elementAt(1)) /
            2;
        final collisionNormal = absoluteCenter - mid;
        final separationDistance = (size.x / 2) - collisionNormal.length;
        collisionNormal.normalize();

        if (fromAbove.dot(collisionNormal) > 0.9) {
          isOnGround = true;
        }

        position += collisionNormal.scaled(separationDistance);
      }
    }

    if (other is Star) {
      other.removeFromParent();
      game.starsCollected++;
    }

    if (other is WaterEnemy) {
      hit();
    }
    super.onCollision(intersectionPoints, other);
  }
}
