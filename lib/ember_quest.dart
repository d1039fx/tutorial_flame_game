import 'package:flame/events.dart';
import 'package:flame/game.dart';
import 'package:platform_game/actors/ember.dart';
import 'package:platform_game/actors/water_enemy.dart';
import 'package:platform_game/managers/segment_manager.dart';
import 'package:platform_game/objects/ground_block.dart';
import 'package:platform_game/objects/platform_block.dart';
import 'package:flutter/material.dart';

import 'lib/overlays/hud.dart';
import 'objects/star.dart';

class EmberQuestGame extends FlameGame
    with HasCollisionDetection, HasKeyboardHandlerComponents {
  late EmberPlayer _ember;
  double objectSpeed = 0.0;
  final Vector2 velocity = Vector2.zero();

  late double lastBlockXPosition = 0.0;
  late UniqueKey lastBlockKey;
  int starsCollected = 0;
  int health = 3;
  bool loadHubState = false;

  void loadGameSegments(int segmentIndex, double xPositionOffset) {
    for (final block in segments[segmentIndex]) {
      switch (block.blockType) {
        case GroundBlock:
          add(GroundBlock(
              gridPosition: block.gridPosition, xOffset: xPositionOffset));
          break;
        case PlatformBlock:
          add(PlatformBlock(
              gridPosition: block.gridPosition, xOffset: xPositionOffset));
          break;
        case Star:
          add(Star(gridPosition: block.gridPosition, xOffset: xPositionOffset));
          break;
        case WaterEnemy:
          add(WaterEnemy(
              gridPosition: block.gridPosition, xOffset: xPositionOffset));
          break;
      }
    }
  }

  void initializeGame(bool loadHub) {
    final segmentsToLoad = (size.x / 640).ceil();
    segmentsToLoad.clamp(0, segments.length);

    for (var i = 0; i <= segmentsToLoad; i++) {
      loadGameSegments(i, (640 * i).toDouble());
    }

    _ember = EmberPlayer(position: Vector2(128, canvasSize.y - 128));
    add(_ember);
    if (loadHub) {
      loadHubState = true;
      add(Hud());
    }
  }

  void reset() {
    starsCollected = 0;
    health = 3;
    initializeGame(false);
    loadHubState = false;
  }

  @override
  Future<void>? onLoad() async {
    // TODO: implement onLoad
    await images.loadAll([
      'block.png',
      'ember.png',
      'ground.png',
      'heart.png',
      'heart_half.png',
      'star.png',
      'water_enemy.png'
    ]);
    initializeGame(true);
    return super.onLoad();
  }

  @override
  void update(double dt) {
    // TODO: implement update
    if (health <= 0) {
      overlays.add('GameOver');
    }
    super.update(dt);
  }

  @override
  Color backgroundColor() {
    // TODO: implement backgroundColor
    return const Color.fromARGB(255, 173, 223, 247);
  }
}
