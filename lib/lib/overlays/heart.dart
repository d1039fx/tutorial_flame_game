import 'package:flame/components.dart';
import 'package:platform_game/ember_quest.dart';

enum HeartState { available, unavailable }

class HeartHealthComponent extends SpriteGroupComponent<HeartState>
    with HasGameRef<EmberQuestGame> {
  final int heartNumber;

  HeartHealthComponent(
      {required this.heartNumber,
      required super.size,
      required super.position,
      super.scale,
      super.angle,
      super.anchor,
      super.priority});

  @override
  Future<void>? onLoad() async {
    // TODO: implement onLoad
    final availableSprite =
        await game.loadSprite('heart.png', srcSize: Vector2.all(32));
    final unavailableSprite = await game.loadSprite('heart_half.png');

    sprites = {
      HeartState.available : availableSprite,
      HeartState.unavailable : unavailableSprite
    };
    current = HeartState.available;
    await super.onLoad();
  }

  @override
  void update(double dt) {
    // TODO: implement update
    if(game.health < heartNumber){
      current = HeartState.unavailable;
    }else{
      current = HeartState.available;
    }
    super.update(dt);
  }
}
