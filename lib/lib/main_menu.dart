import 'package:flutter/material.dart';
import 'package:platform_game/ember_quest.dart';

class MainMenu extends StatelessWidget {
  final EmberQuestGame game;
  const MainMenu({Key? key, required this.game}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const blackTextColor = Color.fromRGBO(0, 0, 0, 1.0);
    const whiteTextColor = Color.fromRGBO(255, 255, 255, 1.0);

    return Material(
      color: Colors.transparent,
      child: Center(
        child: Container(
          padding: const EdgeInsets.all(10.0),
          height: 250,
          width: 300,
          decoration: const BoxDecoration(
              color: blackTextColor,
              borderRadius: BorderRadius.all(Radius.circular(20))),
          child: Column(
            children: [
              const Text(
                'Ember Quest',
                style: TextStyle(color: whiteTextColor, fontSize: 24),
              ),
              const SizedBox(
                height: 40,
              ),
              SizedBox(
                width: 200,
                height: 75,
                child: ElevatedButton(
                    onPressed: () {
                      game.overlays.remove('MainMenu');
                      game.loadHubState = false;
                    },
                    child: const Text(
                      'Play',
                      style: TextStyle(fontSize: 40.0, color: blackTextColor),
                    )),
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                'Use WASD or Arrow Keys for movement. Space bar to jump. Collect as many stars as you can and avoid enemies!',
                style: TextStyle(color: whiteTextColor, fontSize: 14),
                textAlign: TextAlign.center,
              )
            ],
          ),
        ),
      ),
    );
  }
}
